export interface TChannel {
  feed: {
    link: string
    url: string
    title: string
  };

  items: TFeedItem[];
}

export interface TFeedItem {
  title: string;
  description: string;
  author: string;
  categories: string[];
  content: string;
  enclosure: {
    link: string,
    type: string
  };
  guid: string;
  link: string;
  pubDate: string;
}
