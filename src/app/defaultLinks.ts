export const defaultLinks = [
  'https://www.gamespot.com/feeds/mashup/',
  'http://feeds.feedburner.com/oreilly/radar/atom',
  'https://www.pcworld.com/index.rss',
  'https://www.reddit.com/.rss',
  'https://www.vz.ru/export/yandex.xml'
];
