import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RssEntryComponent } from './rss-entry.component';

describe('RssEntryComponent', () => {
  let component: RssEntryComponent;
  let fixture: ComponentFixture<RssEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RssEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RssEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
