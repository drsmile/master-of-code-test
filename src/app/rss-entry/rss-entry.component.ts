import { Component, OnInit } from '@angular/core';
import { ShareService } from '../../providers/share.service';
import { TChannel, TFeedItem } from '../../types/channel';

@Component({
  selector: 'app-rss-entry',
  templateUrl: './rss-entry.component.html',
  styleUrls: ['./rss-entry.component.css']
})
export class RssEntryComponent implements OnInit {
  public channel: TChannel;
  public filtering = false;
  private readonly hiddenFeedKey = 'hiddenFeed';
  private hiddenFeed = JSON.parse(localStorage.getItem(this.hiddenFeedKey)) || {};

  constructor(
    private share: ShareService
  ) {
  }

  ngOnInit() {
    this.share.value$.subscribe((value: TChannel) => {
      this.channel = value;
    });

    this.share.filtering$.subscribe((value: boolean) => {
      this.filtering = value;
    });
  }

  public toggleItem(item: TFeedItem) {
    const channelName = this.channel.feed.link;
    const hiddenFeed = this.hiddenFeed[channelName];

    if (hiddenFeed) {
      const indexOfItem = hiddenFeed.indexOf(item.guid);
      if (indexOfItem !== -1) {
        this.hiddenFeed[channelName].splice(indexOfItem, 1);
      } else {
        this.hiddenFeed[channelName].push(item.guid);
      }
    } else {
      this.hiddenFeed[channelName] = [item.guid];
    }

    localStorage.setItem(this.hiddenFeedKey, JSON.stringify(this.hiddenFeed));
  }

  public checkIsItemHidden(item: TFeedItem) {
    const channelName = this.channel.feed.link;
    const hiddenFeed = this.hiddenFeed[channelName] || [];

    return hiddenFeed.indexOf(item.guid) !== -1;
  }
}
