import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';

import { RssService } from '../providers/Feed/rss.service';
import { ShareService } from '../providers/share.service';

import { AppComponent } from './app.component';
import { RssListComponent } from './rss-list/rss-list.component';
import { RssEntryComponent } from './rss-entry/rss-entry.component';
import { AppHeaderComponent } from './app-header/app-header.component';

@NgModule({
  declarations: [
    AppComponent,
    RssListComponent,
    RssEntryComponent,
    AppHeaderComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule
  ],
  providers: [
    RssService,
    ShareService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
