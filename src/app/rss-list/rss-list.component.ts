import { Component, OnInit } from '@angular/core';
import { RssService } from '../../providers/Feed/rss.service';
import { ShareService } from '../../providers/share.service';
import { validateUrl } from '../../providers/validateUrl';
import { removeArrayItem } from '../../providers/removeArrayItem';
import { defaultLinks } from '../defaultLinks';
import { TChannel } from '../../types/channel';

@Component({
  selector: 'app-rss-list',
  templateUrl: './rss-list.component.html',
  styleUrls: ['./rss-list.component.css']
})
export class RssListComponent implements OnInit {
  public rssList: TChannel[] = [];
  public rssListDefault: TChannel[] = [];
  public channelTitle = '';
  public userInputUrl = '';
  public filtering = true;
  private userChannels: Array<string> = JSON.parse(localStorage.getItem('userChannels')) || [];

  constructor(
    private rss: RssService,
    private share: ShareService,
  ) {
  }

  ngOnInit() {
    this.useDefaultsChannels();
    this.useUsersChannels();
  }

  ngAfterViewInit() {
    this.setFiltering();
  }

  private useUsersChannels() {
    if (this.userChannels.length) {
      this.userChannels.forEach(link => {
        this.rss.getArticles(link).subscribe((channel: TChannel) => {
          this.addRssChannelToList(channel);
          this.setCurrentChannel(this.rssList[0]);
        });
      });
    }
  }

  private useDefaultsChannels() {
    defaultLinks.forEach(link => {
      this.rss.getArticles(link).subscribe((channel: TChannel) => {
        this.addRssChannelToList(channel, true);
        this.setCurrentChannel(this.rssListDefault[0]);
      });
    });
  }

  private addUserChannelToList(url: string = this.userInputUrl) {
    if (this.userChannels.indexOf(url) !== -1) {
      alert(`You've added this link`);
      return;
    }

    if (!validateUrl(url)) {
      alert('Sorry, try another URL');
      this.userInputUrl = '';
      return;
    }

    this.userChannels.push(url);
    this.rss.getArticles(url).subscribe((channel) => {
      this.addRssChannelToList(channel);
    });

    localStorage.setItem('userChannels', JSON.stringify(this.userChannels));
    this.userInputUrl = '';
  }

  private removeUserChannel(e, url: string) {
    e.stopPropagation();
    removeArrayItem(this.userChannels, url);
    localStorage.setItem('userChannels', JSON.stringify(this.userChannels));
    this.rssList.forEach((item: TChannel, i) => {
      if (item.feed.url === url) {
        this.rssList.splice(i, 1);
      }
    });
  }

  private setCurrentChannel(item: TChannel): void {
    this.share.next(item);
    if (item.feed) {
      if (item.feed.title) {
        this.channelTitle = item.feed.title;
      } else {
        this.channelTitle = item.feed.url;
      }
    }
  }

  private setFiltering(): void {
    this.share.nextFiltering(this.filtering);
  }

  private addRssChannelToList(item: TChannel, isDefaultChannel: boolean = false): void {
    if (Object.keys(item).length === 0) {
      return;
    }

    const list = isDefaultChannel ? this.rssListDefault : this.rssList;
    let found = false;

    list.some((value, i) => {
      if (item.feed.url === value.feed.url) {
        list[i] = item;
        found = true;
        return true;
      }
    });

    if (!found) {
      list.push(item);
    }

    if (isDefaultChannel) {
      this.rssListDefault = list;
    } else {
      this.rssList = list;
    }
  }
}
