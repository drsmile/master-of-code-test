import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class ShareService {
  public value$: Observable<any>;
  public filtering$: Observable<any>;
  private nextSubject = new Subject<any>();
  private nextSubjectFiltering = new Subject<any>();

  constructor() {
    this.value$ = this.nextSubject.asObservable();
    this.filtering$ = this.nextSubjectFiltering.asObservable();
  }

  public next(data) {
    this.nextSubject.next(data);
  }

  public nextFiltering(data) {
    this.nextSubjectFiltering.next(data);
  }
}
