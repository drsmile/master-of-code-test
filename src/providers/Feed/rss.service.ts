import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { getRootDomain } from './getDomainName';
import { BehaviorSubject } from 'rxjs';
import { TChannel } from '../../types/channel';

@Injectable()
export class RssService {
  private readonly RSS2JSON_LINK = 'https://api.rss2json.com/v1/api.json?rss_url=';
  constructor(
    public http: HttpClient,
  ) {}

  public getArticles(url: string): BehaviorSubject<TChannel> {
    const feedDomainName = getRootDomain(url);
    const rssChannelData$: any = new BehaviorSubject({});
    rssChannelData$.next(this.getSaved(feedDomainName, url));

    this.http.get(this.RSS2JSON_LINK + url, { responseType: 'json' }).subscribe((channel) => {
      this.setSaved(feedDomainName, channel);
      rssChannelData$.next({ ...channel, link: url, updated: true });
    }, error => {
      alert(feedDomainName + ' - ' + error.error.message);
      rssChannelData$.next({ ...rssChannelData$.value, feed: { title: feedDomainName, url }, updated: undefined });
    });

    return rssChannelData$;
  }

  private getSaved(feedDomainName, spareUrl) {
    if (feedDomainName) {
      const items = JSON.parse(localStorage.getItem(`feed-${feedDomainName}`));
      if (items !== undefined && items !== null) {
        return ({ ...items, updated: false, feed: { url: spareUrl } });
      } else {
        return { feed: { url: spareUrl }, updated: false };
      }
    }

    return {};
  }

  private setSaved(feedDomainName, feed) {
    if (feedDomainName) {
      localStorage.setItem(`feed-${feedDomainName}`, JSON.stringify(feed));
    }
  }
}
