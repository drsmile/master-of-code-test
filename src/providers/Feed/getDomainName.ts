export function getHostname(url: string): string {
  let hostname;

  if (url.indexOf('//') !== -1) {
    hostname = url.split('/')[2];
  } else {
    hostname = url.split('/')[0];
  }

  hostname = hostname.split(':')[0];
  hostname = hostname.split('?')[0];

  return hostname;
}

// To address those who want the "root domain," use this function:
export function getRootDomain(url: string): string {
  let domain = getHostname(url);
  const splitArr = domain.split('.');
  const arrLen = splitArr.length;

  if (arrLen > 2) {
    domain = splitArr[arrLen - 2] + '.' + splitArr[arrLen - 1];
    if (splitArr[arrLen - 2].length === 2 && splitArr[arrLen - 1].length === 2) {
      domain = splitArr[arrLen - 3] + '.' + domain;
    }
  }
  return domain;
}
