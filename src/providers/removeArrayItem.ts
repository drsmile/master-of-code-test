export function removeArrayItem(array, element) {
  const index = array.indexOf(element);
  array.splice(index, 1);
}
