export function validateUrl(url) {
  return /^(ftp|http|https):\/\/[^ "]+$/.test(url);
}
